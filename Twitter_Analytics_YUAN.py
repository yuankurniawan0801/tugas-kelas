#!/usr/bin/env python
# coding: utf-8

# In[1]:


consumer_key = 'P4opscCYayfeU7OQFnkqh7WYQ'
consumer_secret = 'g2pi4dzjfOYYSoxWyZWHmtlSjUicAizfaKiVM20zvAOQJ8eNo1'
access_token = '1167359584446513152-HTnVMVfwS0AaJyMLGvmA4genAT2ewO'
access_token_secret = 'fnC3UhDvrDox9XGT6pPivRQYk42Wpt8XK7V3VUrzllAFQ'


# In[2]:


import tweepy
import json
import datetime

auth = tweepy.OAuthHandler(consumer_key,consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth)


# In[3]:


date_since = datetime.datetime(2020,6,11,0,0,0)
date_until = datetime.datetime(2020,6,10,0,0,0)

search_terms = ['Covid19','Corona']


def stream_tweets(search_term):
    data = []
    for tweet in tweepy.Cursor(api.search, q='\"{}\" -filter:retweets'.format(search_term), count=100, lang='en', since = "2020-02-11", until = "2020-06-10", tweet_mode = 'extended').items():
        tweet_details = {}
        tweet_details['name'] = tweet.user.screen_name
        tweet_details['retweets'] = tweet.full_text
        tweet_details['location'] = tweet.user.location
        tweet_details['created'] = tweet.created_at.strftime("%d-%b-%Y")
        tweet_details['followers'] = tweet.user.followers_count
        tweet_details['is_user_verified'] = tweet.user.verified
        data.append(tweet_details)
        
                
    with open('data/{}.json'.format(search_term),'w') as f:
        json.dump(data,f)
    print('done!')


# In[ ]:


if __name__ == "__main__":
    print('Starting to stream...')
    for search_term in search_terms:
        stream_tweets(search_term)
    print('finished!')


# In[4]:


import pandas as pd
import numpy as np
import re

def clean_tweet(tweet):
    return ' '.join(re.sub('(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)', ' ', tweet).split())


# In[5]:


import pandas as pd
df = pd.read_json (r'C:\Users\ASUS\Documents\Latihanpython\data\Corona.json')
df.to_csv (r'C:\Users\ASUS\Documents\Latihanpython\data\Corona.csv', index = None)


# In[6]:


df = pd.read_json (r'C:\Users\ASUS\Documents\Latihanpython\data\Covid19.json')
df.to_csv (r'C:\Users\ASUS\Documents\Latihanpython\data\Covid19.csv', index = None)


# In[7]:


def created_covid19_df():
    covid19_df = pd.read_json('data/Covid19.json', orient='records')
    covid19_df['clean_tweet'] = covid19_df['tweet'].apply(lambda x: clean_tweet(x))
    covid19_df['search_name'] = 'Covid19'
    covid19_df.drop_duplicates(subset=['name'], keep='first', inplace=True)
    return covid19_df

def created_corona_df():
    corona_df = pd.read_json('data/Corona.json',orient='records')
    corona_df['clean_tweet'] = corona_df['tweet'].apply(lambda x: clean_tweet(x))
    corona_df['search_name'] = 'Corona'
    corona_df.drop_duplicates(subset=['name'], keep='first', inplace=True)
    return corona_df


# In[8]:


def join_dfs():
    covid19_df = created_covid19_df()
    corona_df = created_corona_df()
    frames = [covid19_df,corona_df]
    searchname_df = pd.concat(frames, ignore_index=True)
    return searchname_df


# In[29]:


def analyze():
    searchname_df = join_dfs()
    tweet_by_keyword = searchname_df.groupby('search_name')['retweets'].mean().reset_index()
    tweet_by_keyword.columns = ['keyword_name', 'average_of_tweets']
    
    followers_of_user_by_sn = searchname_df.groupby('search_name')['followers'].mean().reset_index()
    followers_of_user_by_sn.columns = ['keyword_name', 'average_no_of_followers_of_user']
    
    tweet_by_location = searchname_df.groupby('search_name')['location'].count().reset_index()
    tweet_by_location.columns = ['location', 'amount_of_location']
    return (tweet_by_keyword, followers_of_user_by_sn, tweet_by_location)


# In[33]:


from matplotlib import pyplot as plt

def plot_graphs():
    analysis_details = analyze()
    tweet_by_searchname, average_follower, tweet_location = analysis_details
    
    fig1, ax1 = plt.subplots()
    ax1.bar(tweet_by_searchname['keyword_name'], tweet_by_searchname['average_of_tweets'], label='tweets by keyword_name')
    #ax1.set_xlablel('Keyword')
    ax1.set_ylabel('Average of tweets')
    ax1.set_title('Average of Retweets Search by Keyword')
    
    fig2, ax2 = plt.subplots()
    ax2.bar(average_follower['keyword_name'], average_follower['average_no_of_followers_of_user'], label = 'tweets by keyword_name')
    #ax2.set_xlabel('Keyword')
    ax2.set_ylabel('Average of followers')
    ax2.set_title('Average of User Follower by Keyword')
       
    list_of_figures = [plt.figure(i) for i in plt.get_fignums()]
    return list_of_figures


# In[34]:


plot_graphs()


# In[49]:


import matplotlib.pyplot as plt

Corona = pd.read_csv('Corona.csv')

Corona1 = Corona[['location', 'name']].groupby('location')
Corona2 = Corona1.count().reset_index()

plt.figure(figsize=(60,250))

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 30}

plt.rc('font', **font)

plt.barh(Corona2.location, Corona2.name)

plt.show()


# In[53]:


import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

plt.figure(figsize=(13,5))

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 10}

plt.rc('font', **font)

Corona = pd.read_csv('Corona.csv')
Covid19 = pd.read_csv('Covid19.csv')

Corona1 = Corona[['name','created']].groupby('created')
Corona2 = Corona1.count()

Covid19_1 = Covid19[['name','created']].groupby('created')
Covid19_2 = Covid19_1.count()

plt.title('Amount of tweet created by time')

plt.plot(Corona2.name, label = 'Corona')
plt.plot(Covid19_2.name, label = 'Covid19')

plt.xlabel('Date')
plt.ylabel('Amount')
plt.legend()


# In[ ]:




