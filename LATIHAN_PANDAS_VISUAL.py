#!/usr/bin/env python
# coding: utf-8

# In[85]:


import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
import numpy as np


# In[87]:


x = np.linspace(0, 5, 11)
y = x ** 2


# In[88]:


x


# In[89]:


y


# In[91]:


plt.plot(x, y, 'r')
plt.xlabel('Y Axist Title Here')
plt.ylabel('Y Axis Title Here')
plt.title('String Title Here')
plt.show()


# In[92]:


plt.subplot(1, 2, 1)
plt.plot(x, y, 'r--')
plt.subplot(1, 2, 2)
plt.plot(y, x, 'g*-');


# In[94]:


fig = plt.figure()

axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])

axes.plot(x, y, 'b')
axes.set_xlabel('Set X Label')
axes.set_ylabel('Set Y Label')
axes.set_title('Set Title')


# In[95]:


fig = plt.figure()

axes1 = fig.add_axes([0.1, 0.1, 0.8, 0.8])
axes2 = fig.add_axes([0.2, 0.5, 0.4, 0.3])

axes1.plot(x, y, 'b')
axes1.set_xlabel('X_label_axes2')
axes1.set_ylabel('Y_Lavel_axes2')
axes1.set_title('Axes 2 Title')

axes2.plot(y, x, 'r')
axes2.set_xlabel('X_Label_axes2')
axes2.set_ylabel('Y_Label_axes2')
axes2.set_title('Axes 2 Title')


# In[97]:


fig, axes = plt.subplots()

axes.plot(x, y, 'r')
axes.set_xlabel('x')
axes.set_ylabel('y')
axes.set_title('title');


# In[98]:


fig, axes = plt.subplots(nrows = 1, ncols = 2)


# In[99]:


axes


# In[100]:


for ax in axes:
    ax.plot(x, y, 'b')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('title')

fig


# In[101]:


fig, axes = plt.subplots(nrows = 1,ncols = 2)

for ax in axes:
    ax.plot(x, y, 'g')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('title')
    
fig
plt.tight_layout()


# In[103]:


fig = plt.figure(figsize = (8, 4), dpi = 100)


# In[104]:


fig, axes = plt.subplots(figsize=(12, 3))

axes.plot(x, y, 'r')
axes.set_xlabel('x')
axes.set_ylabel('y')
axes.set_title('title');


# In[106]:


fig.savefig('filename.png')


# In[107]:


fig.savefig("filename.png", dpi = 200)


# In[109]:


ax.set_title('title');


# In[110]:


ax.set_xlabel('x')
ax.set_ylabel('y');


# In[112]:


fig = plt.figure()

ax = fig.add_axes([0, 0, 1, 1])

ax.plot(x, x**2, label="x**2")
ax.plot(x, x**3, label="x**3")


# In[114]:


fig, ax = plt.subplots()
ax.plot(x, x**2, 'b.-')
ax.plot(x, x**3, 'g--')


# In[116]:


fig, ax = plt.subplots()

ax.plot(x, x+1, color = "blue", alpha = 0.5)
ax.plot(x, x+2, color = "#8B008B")
ax.plot(x, x+3, color = "#FF8C00")


# In[118]:


fig, ax = plt.subplots(figsize=(12, 6))

ax.plot(x, x+1, color="red", linewidth = 0.25)
ax.plot(x, x+2, color="red", linewidth = 0.50)
ax.plot(x, x+3, color="red", linewidth = 1.00)
ax.plot(x, x+4, color="red", linewidth = 2.00)

ax.plot(x, x+5, color="green", lw=3, linestyle='-')
ax.plot(x, x+6, color="green", lw=3, ls='-.')
ax.plot(x, x+7, color="green", lw=3, ls=":")

line, = ax.plot(x, x+8, color="black", lw=1.50)
line.set_dashes([5, 10, 15, 10])

ax.plot(x, x+9, color="blue", lw=3, ls='-', marker='+')
ax.plot(x, x+10, color="blue", lw=3, ls='--', marker='o')
ax.plot(x, x+11, color="blue", lw=3, ls='-', marker='s')
ax.plot(x, x+12, color="blue", lw=3, ls='--', marker='1')

ax.plot(x, x+13, color="purple", lw=1, ls='-', marker='o', markersize=2)
ax.plot(x, x+14, color="purple", lw=1, ls='-', marker='o', markersize=4)
ax.plot(x, x+15, color="purple", lw=1, ls='-', marker='o', markersize=8, markerfacecolor = 'red')
ax.plot(x, x+16, color="purple", lw=1, ls='-', marker='s', markersize=8,
       markerfacecolor='yellow', markeredgewidth=3, markeredgecolor='green')


# In[120]:


fig, axes = plt.subplots(1, 3, figsize=(12, 4))

axes[0].plot(x, x**2, x, x**3)
axes[0].set_title("default axes ranges")

axes[1].plot(x, x**2, x, x**3)
axes[1].axis('tight')
axes[1].set_title("tight axes")

axes[2].plot(x, x**2, x, x**3)
axes[2].set_ylim([0, 60])
axes[2].set_xlim([2, 5])
axes[2].set_title("custom axes range");


# In[122]:


plt.scatter(x, y)


# In[123]:


from random import sample
data = sample(range(1, 1000), 100)
plt.hist(data)


# In[124]:


data = [np.random.normal(0, std, 100) for std in range(1, 4)]

plt.boxplot(data, vert=True, patch_artist=True)


# In[127]:


import numpy as np
import pandas as pd

get_ipython().run_line_magic('matplotlib', 'inline')


# In[128]:


df1 = pd.read_csv('df1', index_col=0)
df2 = pd.read_csv('df2')


# In[129]:


df1['A'].hist()


# In[130]:


import matplotlib.pyplot as plt
plt.style.use('ggplot')


# In[131]:


df1['A'].hist()


# In[132]:


plt.style.use('bmh')
df1['A'].hist()


# In[133]:


plt.style.use('dark_background')
df1['A'].hist()


# In[134]:


plt.style.use('fivethirtyeight')
df1['A'].hist()


# In[136]:


df1['A'].hist()


# In[137]:


plt.style.use('ggplot')


# In[138]:


df2.head()


# In[139]:


df2.plot.bar()


# In[140]:


df2.plot.bar(stacked=True)


# In[141]:


df2.plot.area(alpha=0.4)


# In[143]:


df1['A'].plot.hist(bins=50)


# In[147]:


df1.plot.line(x=df1.index,y='B', figsize=(12,3), lw=1)


# In[148]:


df1.plot.scatter(x='A', y='B')


# In[149]:


df1.plot.scatter(x='A',y='B',c='C',cmap = 'coolwarm')


# In[150]:


df1.plot.scatter(x='A', y='B',s=df1['C']*200)


# In[151]:


df2.plot.box()


# In[153]:


df = pd.DataFrame(np.random.randn(1000, 2), columns=['a', 'b'])
df.plot.hexbin(x='a', y='b', gridsize=25, cmap='Oranges')


# In[154]:


df2['a'].plot.kde()


# In[155]:


df2.plot.density()


# In[ ]:


import pandas as pd
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib.inline', '')


# In[157]:


mcdon = pd.read_csv('mcdonalds.csv',index_col = 'Date', parse_dates = True)


# In[158]:


mcdon.head()


# In[159]:


mcdon.plot()


# In[160]:


mcdon['Adj. Close'].plot(figsize=(12, 8))


# In[163]:


mcdon['Adj. Close'].plot(figsize=(12,8))
plt.ylabel('Close Price')
plt.xlabel('Overwrite Data Index')
plt.title('Mcdonalds')


# In[164]:


mcdon['Adj. Close'].plot(figsize=(12,88), title='Pandas Title')


# In[165]:


mcdon['Adj. Close'].plot(xlim=['2007-01-01', '2009-01-01'])


# In[166]:


mcdon['Adj. Close'].plot(xlim=['2007-01-01', '2009-01-01'], ylim=[0, 50])


# In[173]:


mcdon['Adj. Close'].plot(xlim=['2007-01-01', '2007-05-01'], ylim=[0, 40], ls='--', c='r')


# In[177]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as dates

mcdon['Adj. Close'].plot(xlim=['2007-01-01', '2007-05-01'], ylim=[0, 40])


# In[181]:


idx = mcdon.loc['2007-01-01':'2007-05-01'].index
stock = mcdon.loc['2007-01-01':'2007-05-01']['Adj. Close']


# In[184]:


idx


# In[182]:


stock


# In[183]:


fig, ax = plt.subplots()
ax.plot_date(idx, stock, '-')
plt.tight_layout()
plt.show()


# In[188]:


fig, ax = plt.subplots()
ax.plot_date(idx, stock, '-')

fig.autofmt_xdate()
plt.tight_layout()
plt.show()


# In[189]:


fig, ax = plt.subplots()
ax.plot_date(idx, stock, '-')
ax.yaxis.grid(True)
ax.xaxis.grid(True)
fig.autofmt_xdate()
plt.tight_layout()
plt.show()


# In[191]:


fig, ax = plt.subplots()
ax.plot_date(idx, stock, '-')

ax.yaxis.grid(True)
ax.xaxis.grid(True)

ax.xaxis.set_major_locator(dates.MonthLocator())
ax.xaxis.set_major_formatter(dates.DateFormatter('%b\n%Y'))

fig.autofmt_xdate()
plt.tight_layout()
plt.show()


# In[193]:


fig, ax = plt.subplots()
ax.plot_date(idx, stock, '-')

ax.yaxis.grid(True)
ax.xaxis.grid(True)

ax.xaxis.set_major_locator(dates.MonthLocator())
ax.xaxis.set_major_formatter(dates.DateFormatter('\n\n\n\n%Y--%B'))

fig.autofmt_xdate()
plt.tight_layout()
plt.show()


# In[196]:


fig, ax = plt.subplots()
ax.plot_date(idx, stock, '-')


ax.xaxis.set_major_locator(dates.MonthLocator())
ax.xaxis.set_major_formatter(dates.DateFormatter('\n\n%Y--%B'))

ax.xaxis.set_minor_locator(dates.WeekdayLocator())
ax.xaxis.set_minor_formatter(dates.DateFormatter('%d'))

ax.yaxis.grid(True)
ax.xaxis.grid(True)

fig.autofmt_xdate()
plt.tight_layout()
plt.show()


# In[198]:


fig, ax = plt.subplots(figsize = (10,8))
ax.plot_date(idx, stock, '-')
             
ax.xaxis.set_major_locator(dates.WeekdayLocator(byweekday=1))
ax.xaxis.set_major_formatter(dates.DateFormatter('%B-%d-%a'))

ax.yaxis.grid(True)
ax.xaxis.grid(True)

fig.autofmt_xdate()

plt.tight_layout()
plt.show()

