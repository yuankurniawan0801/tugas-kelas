#!/usr/bin/env python
# coding: utf-8

# In[2]:


import numpy as np
import pandas as pd


# In[3]:


labels = ['a','b','c']
my_list = [10,20,30]
arr = np.array ([10,20,30])
d = {'a':10,'b':20, 'c':30}


# In[4]:


pd.Series(data=my_list)


# In[5]:


pd.Series(data=my_list,index=labels)


# In[6]:


pd.Series(my_list,labels)


# In[7]:


pd.Series(arr)


# In[8]:


pd.Series(arr,labels)


# In[9]:


pd.Series(d)


# In[10]:


pd.Series(data=labels)


# In[11]:


pd.Series([sum,print,len])


# In[24]:


ser1 = pd.Series([1,2,3,4], index = ['USA','Germany','USSR','Japan'])


# In[25]:


ser1


# In[19]:


ser2 = pd.Series([1,2,5,4], index = ['USA','Germany','Italy','Japan'])


# In[21]:


ser2


# In[22]:


ser1['USA']


# In[26]:


ser1 + ser2


# In[28]:


from numpy.random import randn
np.random.seed(101)


# In[29]:


df = pd.DataFrame(randn(5,4),index='A B C D E'.split(),columns = 'W X Y Z'.split())


# In[30]:


df


# In[31]:


df ['W']


# In[32]:


df[['W','Z']]


# In[33]:


df.W


# In[34]:


type (df['W'])


# In[36]:


df['new'] = df['W']+df['Y']


# In[37]:


df


# In[38]:


df.drop('new',axis=1)


# In[39]:


df


# In[40]:


df.drop('new',axis=1,inplace=True)


# In[41]:


df


# In[42]:


df.drop('E',axis=0)


# In[43]:


df.loc['A']


# In[44]:


df.iloc[2]


# In[45]:


df.loc['B','Y']


# In[46]:


df.loc[['A','B'],['W','Y']]


# In[47]:


df


# In[48]:


df>0


# In[49]:


df[df>0]


# In[50]:


df[df['W']>0]


# In[51]:


df[df['W']>0]['Y']


# In[53]:


df[df['W']>0][['Y','X']]


# In[54]:


df[(df['W']>0) & (df['Y'] > 1)]


# In[55]:


df


# In[56]:


df.reset_index()


# In[58]:


newind = 'CA NY WY OR CO'.split()


# In[59]:


df['States'] = newind


# In[60]:


df


# In[61]:


df.set_index('States')


# In[62]:


df


# In[63]:


df.set_index('States',inplace=True)


# In[64]:


df


# In[69]:


outside = ['G1', 'G1', 'G1', 'G2', 'G2', 'G2']
inside = [1,2,3,1,2,3]
hier_index = list(zip(outside,inside))
hier_index = pd.MultiIndex.from_tuples(hier_index)


# In[71]:


hier_index


# In[75]:


df = pd.DataFrame(np.random.randn(6,2), index = hier_index,columns=['A','B'])
df


# In[76]:


df.loc['G1']


# In[77]:


df.loc['G1'].loc[1]


# In[78]:


df.index.names


# In[79]:


df.index.names = ['Group','Num']


# In[80]:


df


# In[81]:


df.xs('G1')


# In[82]:


df.xs(['G1',1])


# In[83]:


df = pd.DataFrame({'A':[1,2,np.nan],'B':[5,np.nan,np.nan],'C':[1,2,3]})


# In[84]:


df


# In[85]:


df.dropna()


# In[86]:


df.dropna(axis=1)


# In[87]:


df.dropna(thresh=2)


# In[88]:


df.fillna(value='FILL VALUE')


# In[89]:


df['A'].fillna(value=df['A'].mean())


# In[90]:


data = {'Company':['GOOG','GOOG','MFST','MFST','FB','FB'],'Person':['Sam','Charlie','Amy','Vanessa','Carl','Sarah'],'Sales':[200,120,340,124,243,350]}


# In[91]:


df = pd.DataFrame(data)


# In[92]:


df


# In[93]:


df.groupby('Company')


# In[94]:


by_com = df.groupby('Company')


# In[95]:


by_com.mean()


# In[96]:


df.groupby('Company').mean()


# In[97]:


by_com.std()


# In[98]:


by_com.min()


# In[99]:


by_com.max()


# In[100]:


by_com.count()


# In[101]:


by_com.describe()


# In[102]:


by_com.describe().transpose()


# In[103]:


by_com.describe().transpose()['GOOG']


# In[104]:


df1 = pd.DataFrame({'A':['A0','A1', 'A2', 'A3'],
                    'B':[ 'B0', 'B1', 'B2', 'B3'],
                    'C':['C0','C1','C2','C3'], 
                    'D':['D0','D1','D2','D3']},
                   index = [0,1,2,3])


# In[105]:


df2 = pd.DataFrame({'A':['A4','A5', 'A6', 'A7'],
                    'B':[ 'B4', 'B5', 'B6', 'B7'],
                    'C':['C4','C5','C6','C7'], 
                    'D':['D4','D5','D6','D7']},
                   index = [4,5,6,7])


# In[106]:


df3 = pd.DataFrame({'A':['A8','A9', 'A10', 'A11'],
                    'B':[ 'B8', 'B9', 'B10', 'B11'],
                    'C':['C8','C9','C10','C11'], 
                    'D':['D8','D9','D10','D11']},
                   index = [8,9,10,11])


# In[108]:


df1


# In[109]:


pd.concat([df1,df2,df3])


# In[112]:


pd.concat([df1,df2,df3], axis=1)


# In[113]:


left = pd.DataFrame({'key':['K0','K1','K2','K3'],
                     'A':['A0','A1', 'A2', 'A3'],
                    'B':[ 'B0', 'B1', 'B2', 'B3']})
right = pd.DataFrame({'key':['K0','K1','K2','K3'],
                     'C':['C0','C1', 'C2', 'C3'],
                    'D':[ 'D0', 'D1', 'D2', 'D3']})  


# In[114]:


left


# In[115]:


right


# In[116]:


pd.merge(left,right,how='inner',on='key')


# In[117]:


left = pd.DataFrame({'key1':['K0','K0','K1','K2'],
                     'key2':['K0','K1','K0','K1'],
                     'A':['A0','A1', 'A2', 'A3'],
                     'B':[ 'B0', 'B1', 'B2', 'B3']})
right = pd.DataFrame({'key1':['K0','K1','K1','K2'],
                      'key2':['K0','K0','K0','K0'],
                      'C':['C0','C1', 'C2', 'C3'],
                      'D':[ 'D0', 'D1', 'D2', 'D3']})  


# In[118]:


pd.merge(left,right,on=['key1','key2'])


# In[119]:


pd.merge(left,right,how='right',on=['key1','key2'])


# In[120]:


pd.merge(left,right,how = 'left', on=['key1','key2'])


# In[123]:


left = pd.DataFrame({'A':['A0','A1', 'A2'],
                    'B':[ 'B0', 'B1', 'B2']},
                   index = ['K0','K2','K3'])
right = pd.DataFrame({'C':['C0','C1', 'C2'],
                    'D':[ 'D0', 'D2', 'D3']},
                   index = ['K0','K2','K3'])


# In[124]:


left.join(right)


# In[125]:


left.join(right, how='outer')


# In[126]:


df = pd.DataFrame({'col1':[1,2,3,4],'col2':[444,555,666,444],'col3':['abc','def','ghi','xyz']})
df.head()


# In[127]:


df['col2'].unique()


# In[128]:


df['col2'].nunique()


# In[129]:


df['col2'].value_counts()


# In[130]:


newdf = df[(df['col1']>2)&(df['col2']==444)]


# In[131]:


newdf


# In[132]:


def times2(x):
    return x*2


# In[133]:


df['col1'].apply(times2)


# In[135]:


df['col3'].apply(len)


# In[136]:


df['col1'].sum()


# In[137]:


del df ['col1']


# In[138]:


df


# In[139]:


df.columns


# In[140]:


df.index


# In[141]:


df


# In[142]:


df.sort_values(by='col2')


# In[144]:


df.isnull()


# In[146]:


df.dropna()


# In[148]:


df = pd.DataFrame({'col1':[1,2,3,np.nan],
                   'col2':[ np.nan,555,666,444],
                   'col3':[ 'abc','def','ghi','xyz']})
df.head()


# In[149]:


df.fillna('FILL')


# In[150]:


data ={'A':['foo','foo','foo', 'bar','bar','bar'],
       'B':[ 'one', 'one', 'two', 'two','one','one'],
       'C':['x','y','x','y','x','y'], 
       'D':[1,3,2,5,4,1]}
df = pd.DataFrame(data)


# In[151]:


df


# In[157]:


df.pivot_table(values='D',index=['A','B'],columns['C'])


# In[159]:


df = pd.read_csv('example')


# In[160]:


df


# In[161]:


df.to_csv('example',index=False)


# In[167]:


pd.read_excel('Excel_Sample.xlsx',sheetname='Sheet1')


# In[4]:


df.to_excel('Excel_sample.xlsx', sheet_name='Sheet1')


# In[6]:


pip install lxml


# In[7]:


pip install html5lib


# In[8]:


pip install BeautifulSoup4


# In[5]:


df = pd.read_html('http //www.fdic.gov/bank/individual/failed/bank list.html')


# In[6]:


df[0]

