#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np


# In[2]:


np.zeros(10)


# In[3]:


np.ones(10)


# In[6]:


np.ones(10)*5


# In[9]:


array=np.arange(10,51)
print(array)


# In[10]:


array=np.arange(10,51,2)
print(array)


# In[17]:


arr= np.arange(9)
arr.reshape(3,3)


# In[18]:


np.eye(3)


# In[20]:


np.random.rand(1)


# In[23]:


np.random.randn(5,5)


# In[36]:


np.arange(0.01,1.01,0.01).reshape(10,10)


# In[37]:


np.linspace(0,1,20)


# In[40]:


mat=np.arange(1,26).reshape(5,5)
mat


# In[50]:


mat=np.arange(1,26).reshape(5,5)
mat[2:,1:]


# In[51]:


mat[3,4]


# In[52]:


mat[:3,1:2]


# In[53]:


mat[4]


# In[54]:


mat[3:]


# In[55]:


mat.sum()


# In[56]:


mat.std()


# In[57]:


mat.sum(axis=0)

