#!/usr/bin/env python
# coding: utf-8

# In[4]:


import pandas as pd
import numpy as np


# In[13]:


df = pd.read_csv('banklist.csv')
df


# In[14]:


df.head()


# In[18]:


df.columns


# In[19]:


df['ST'].nunique()


# In[20]:


df['ST'].unique()


# In[43]:


failed = df[['ST', 'Bank Name']].groupby('ST')
a = failed.count()
a.nlargest(5,['Bank Name'])


# In[51]:


apa = df[['Acquiring Institution', 'Bank Name']].groupby('Acquiring Institution')
b = apa.count()
b.nlargest(5,['Bank Name'])


# In[59]:


df[df['Acquiring Institution'] == 'State Bank of Texas']


# In[70]:


df1 = df[df['ST'] == 'CA'].groupby('City')
df2 = df1.count()
df3 = df2.nlargest(1,['Bank Name'])


df3


# In[75]:


df1 = []

for x in df['Bank Name']:
    if 'Bank' not in x:
        df1.append(x)
len (df1)


# In[77]:


df1 = []

for x in df['Bank Name']:
    if x[0] == 'S':
        df1.append(x)
len (df1)


# In[78]:


df[df['CERT']>20000]['Bank Name'].count()


# In[80]:


df1 = []

for x in df['Bank Name']:
    if len(x.split()) == 2:
        df1.append(x)
        
len(df1)


# In[85]:


df1 = []

for x in df['Closing Date']:
    if x[-2:] == '08':
        df1.append(x)
len(df1)

