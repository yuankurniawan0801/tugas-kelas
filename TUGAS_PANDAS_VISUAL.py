#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
x=np.arange(0,100)
y=x*2
z=x**2


# In[2]:


import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')

plt.show()


# In[50]:


fig = plt.figure()

ax = fig.add_axes([0,0,1,1])
ax.set_title('title')
ax.set_xlabel('X')
ax.set_ylabel('Y')

ax.plot(x,y,'b')


# In[4]:


fig1 = plt.figure()

ax1 = fig1.add_axes([0,0,1,1])
ax2 = fig1.add_axes([0.2,0.5,0.2,0.2])


# In[5]:


fig1 = plt.figure()

ax1 = fig1.add_axes([0,0,1,1])
ax2 = fig1.add_axes([0.2,0.5,0.2,0.2])

ax1.plot(x,y,'r')
ax2.plot(x,y,'r')


# In[6]:


fig1 = plt.figure()
ax0 = fig1.add_axes([0,0,1,1])
ax1 = fig1.add_axes([0.2,0.5,0.4,0.4])


# In[7]:


fig1 = plt.figure()
ax0 = fig1.add_axes([0,0,1,1])
ax1 = fig1.add_axes([0.2,0.5,0.4,0.4])


ax0.set_xlabel('X')
ax0.set_ylabel('Z')

ax1.set_title('Zoom')
ax1.set_xlabel('X')
ax1.set_ylabel('Y')
ax1.set_ylim(30,50)
ax1.set_xlim(20,22)

ax0.plot(x,z,'b')
ax1.plot(x,y,'b')


# In[8]:


fig,axes = plt.subplots(nrows=1,ncols=2)


# In[9]:


fig,axes = plt.subplots(nrows=1,ncols=2)

axes[0].plot(x,y,color='blue',lw="3",ls="--")
axes[1].plot(x,z,color='red',lw="3")


# In[10]:


fig,axes = plt.subplots(nrows=1,ncols=2)

axes[0].plot(x,y,color='blue',lw="3",ls="--")
axes[1].plot(x,z,color='red',lw="3")


# In[11]:


fig,axes = plt.subplots(nrows=1,ncols=2, figsize=(8,2))

axes[0].plot(x,y,color='blue',lw="3")
axes[1].plot(x,z,color='red',lw="3",ls="--")


# In[12]:


import pandas as pd 
import matplotlib.pyplot as plt
df3 = pd.read_csv('df3')


# In[13]:


df3.info()


# In[14]:


df3.head()


# In[23]:


df3.plot.scatter(x='a',y='b',color="red",edgecolor = 'Black', s=40,figsize=(12,3))


# In[33]:


plt.ylabel('Frequency')
df3['a'].hist(color = 'blue', edgecolor='Black')


# In[49]:


plt.ylabel('Frequency')
plt.style.use("ggplot")
df3['a'].hist(bins=30,alpha=0.5, edgecolor='white')


# In[45]:


df3[['a','b']].plot.box(ylim = (0, 1))


# In[35]:


plt.style.use('ggplot')
df3['d'].plot.kde(color = 'red',xlim = (-0.5, 1.5), ylim = (0, 1.2))


# In[34]:


df3['d'].plot.kde(ls="--",lw=2, xlim = (-0.5, 1.5), ylim = (0, 1.2))


# In[22]:


df3.plot.area(xlim = (0, 30), ylim = (0, 3), alpha = 0.5)


# In[ ]:




